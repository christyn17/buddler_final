module Api
  module V1

	class CommentsController < ApplicationController
	
	before_action :find_post
	before_action :find_comment, only: [:edit, :update, :destroy]
	before_action :authenticate_user!

	def create
		@comment = @post.comments.create(comment_params)
		@comment.user_id = current_user.id
		if @comment.save
			redirect_to post_path(@post)
		else
			render 'new'
		end
	end

	def edit
	end

	def update
		if @comment.update(comment_params)
			
		else
			
		end
	end

	def destroy
		
	end

	private

    def comment_params
     
    end

	def find_post
		
	end

	def find_comment
		
	end

	end
	
  end
end