 module Api
  module V1

  class RegistrationsController < Devise::RegistrationsController
  protect_from_forgery
  respond_to :json

  def create
      user = User.new
      user.firstname = params[:firstname]
      user.email = params[:email]
      user.password = params[:password]
      user.password_confirmation = params[:password_confirmation]
      user.image_file_name = params[:image_file_name]
      user.image_content_type = params[:image_content_type]
      user.image_file_size = params[:image_file_size]
      user.image_updated_at = params[:image_updated_at]
      user.created_at = params[:created_at]
      user.updated_at = params[:updated_at]

      if user.save
          render :json=> user.as_json(:email=>user.email), :status=>201
        return
      else
        warden.custom_failure!
        render :json=> user.errors, :status=>422
      end
  end

  private

    def user_params
      params.require(:user).permit(:firstname, :lastname, :email, :password, :password_confirmation)
    end

end

  
  end
end