module Api
  module V1

  class SessionsController < Devise::SessionsController

  protect_from_forgery
  respond_to :json


    def create
      # auth_token = params[:session][:auth_token] if params[:session]
      email = params[:session][:email] if params[:session]
      password = params[:session][:password] if params[:session]

      id = User.find_by(email: email).try(:id) if email.presence

        if email.nil? or password.nil?
          render status: 400, json: { message: 'The request MUST contain the user email and password.' }
        return
        end

      user = User.find_by(email: email)

      if user
        if user.valid_password? password
          user.reset_auth_token!
          # auth_token = set_auth_token
          render status: 200, json: { email: user.email, auth_token: user.auth_token, id: id }
        else
          render status: 401, json: { message: 'Invalid email or password.' }
        end
      else
        render status: 401, json: { message: 'Invalid email or password.' }
      end

      end

      def destroy
        current_user.auth_token = nil
        super
      end

    
  private

    def user_params
      params.require(:user).permit(:email, :password, :auth_token)
    end

    def find_user
      @user = User.find(params[:id])     
    end


    # def set_auth_token

    #   return if current_user.auth_token.present?
    #   current_user.auth_token = generate_auth_token
    # end

    # def generate_auth_token
    #   SecureRandom.uuid.gsub(/\-/,)
    # end

  end

  end
end

