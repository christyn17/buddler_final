class PostsController < ApplicationController

	

	before_action :find_post, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user!, except: [:index, :show] 
	
	def index
		@posts = Post.all.order("created_at DESC")

		#	ids = current_user.friends.pluck(:id) << current_user.id
		#	@posts = Post.where(user_id: ids)
	end

	def show
		
	end

	def new
		@post = current_user.posts.build
	end

	def edit
		@post = Post.find(params[:id])
	end

	def create
		@post = current_user.posts.build(post_params)
		if @post.save
			redirect_to root_path
		else
			render 'new'
		end
	end

	def update
	 	if @post.update(post_params)
			redirect_to post_path
		else
			render 'edit'
		end
	end

	def destroy
   		@post.destroy
   		redirect_to root_path
  end

	private
		def post_params
			params.require(:post).permit(:status)
		end

		def find_post
			@post = Post.find(params[:id])
		end


end
