  class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.


  before_filter :configure_permitted_parameters, if: :devise_controller?
  
  protected
  

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:firstname,:lastname,:email, :password, :password_confirmation, :image) }
    devise_parameter_sanitizer.for(:sign_in) << [:email, :remember_me]
  end

  protect_from_forgery with: :exception

  def update_resource(resource, params)
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end

  def index
      @users = User.search(params[:search])
  end

  def destroy
    current_user[:auth_token] = nil
    resource.soft_delete
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    set_flash_message :notice, :destroyed if is_navigational_format?
    respond_with_navigational(resource){ redirect_to   after_sign_out_path_for(resource_name) }
  end
  	
end
